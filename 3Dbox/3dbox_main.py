import sys
import numpy as np
from argparse import ArgumentParser
from datetime import datetime
from skimage.transform import resize
from copy import deepcopy
from Mihlib import *

from models import Model3D
from keras.callbacks import ModelCheckpoint, LambdaCallback
from keras.optimizers import SGD
from keras import backend as K

def parseArguments():
	parser = ArgumentParser()
	parser.add_argument("type", default="train", type=str)
	parser.add_argument("dataset_path", type=str, help="In test_one mode, this is image path")
	parser.add_argument("--split_train_validate", default=1, type=int)
	parser.add_argument("--split_train_validate_percent", default=5, type=float)
	parser.add_argument("--dir", default="train")
	parser.add_argument("--weights_file", default="model.h5")
	parser.add_argument("--state_file", default="state.pkl")
	parser.add_argument("--num_epochs", default=1, type=int)
	parser.add_argument("--mini_batch_size", type=int, default=20)

	parser.add_argument("--num_neurons", type=int, default=100, help="Number of neurons on all 3 branches.")
	parser.add_argument("--dropout", type=float, default=1, help="Use dropout to the 3 branches.")
	parser.add_argument("--batch_normalization", type=int, default=0, help="Use BN to the 3 branches [0/1]")
	parser.add_argument("--orientation_num_bins", type=int, default=2, help="Number of bins to regress")
	parser.add_argument("--orientation_bins_overlap", type=int, default=20, help="Overlap between bins in angles.")

	args = parser.parse_args()
	assert args.type in ("train", "test", "test_one", "retrain")
	assert args.split_train_validate in (0, 1)

	args.split_train_validate = True if args.split_train_validate == 1 else False
	if args.split_train_validate:
		assert args.split_train_validate_percent > 0 and args.split_train_validate_percent < 100
	assert args.orientation_bins_overlap >= 0 and args.orientation_bins_overlap < 360
	assert args.batch_normalization in (0, 1)
	args.batch_normalization = True if args.batch_normalization == 1 else False
	return args

# The purpose of this is to take KITTIReader's output (uncropped images and dict labels) and turn them into
#  feedable input fot the neural network (each 2d bbox as a separate image and just necessary labels).
class CustomKITTIReader(KITTIReader):
	def __init__(self, datasetPath, splitTrainValidate=True, splitPercent=5, dataAugmentation=[]):
		super().__init__(datasetPath, splitTrainValidate, splitPercent)
		for augmentationType in dataAugmentation:
			assert augmentationType in ("mirror", )
		self.dataAugmentation = dataAugmentation

	def get_image_crop(self, image, bbox_top_left, bbox_bottom_right, shape=(224, 224)):
		# (float, float) -> (int, int) dimensions so we can crop the image
		crop_image = image[bbox_top_left[I] : bbox_bottom_right[I], bbox_top_left[J] : bbox_bottom_right[J]]
		return resize(crop_image, shape, mode="edge")

	# Process each instance of an image separately. Assumes this instance is valid and it's label is good.
	def process_one_instance(self, image, label, binsStarts, binsOverlap):
		instanceLabels = {"size" : None, "orientations" : None, "confidence" : None, "classIndex" : None, \
			"alphaAngle" : None, "bbox_positions" : None, "centers" : None}

		# Class
		objectClass = label["class"]
		objectClassIndex = self.getObjectIndex(objectClass)

		# Sizes
		bbox_dimensions = np.array(label["size"])
		# Train by minimizing class average sizes.
		bbox_dimensions -= self.getObjectSizeMeans(objectClass)

		# Orientation
		# The angle is given in (-180 ; +180) degrees, so change it to (0 ; 360).
		alpha_angle = label["alpha_angle"]
		if alpha_angle < 0:
			alpha_angle += 2 * np.pi
		assert alpha_angle >= 0 and alpha_angle <= 2 * np.pi

		# Crop 2D box
		bbox_positions = np.array(label["2d_box"]).astype(int)
		bbox_top_left, bbox_bottom_right = bbox_positions
		crop_image = self.get_image_crop(image, bbox_top_left, bbox_bottom_right)

		# The alpha_angle is the angle relative to the ray from the camera. The paper generates N
		# overlapping bins for each such angle. Thus, each angle generates N angles which is the
		# displacement from the center of ech bin.
		binsDeplacements, binsConfidence = computeBinDeplacements(alpha_angle, binsStarts, binsOverlap)
		sinDisplacements = np.sin(binsDeplacements)
		cosDisplacements = np.cos(binsDeplacements)
		# The first array has shape 2 x numBins, but we need numBins x 2 for loss computatiion.
		displacements = np.swapaxes(np.array([sinDisplacements, cosDisplacements]), 0, 1)

		instanceLabels["centers"] = label["translation"]
		instanceLabels["bbox_positions"] = bbox_positions
		instanceLabels["size"] = bbox_dimensions
		instanceLabels["orientations"] = displacements
		instanceLabels["confidence"] = binsConfidence
		instanceLabels["classIndex"] = objectClassIndex
		instanceLabels["alphaAngle"] = alpha_angle
		return crop_image, instanceLabels, label["rotation_y_angle"]

	# Given one big image, extract all the boxes and necessary information, based on the labels
	def process_one_image(self, image, label, binsStarts, binsOverlap, testingMode):
		imageLabels = {"size" : [], "orientations" : [], "confidence" : [], "classIndex" : [], "alphaAngle" : [], \
			"bbox_positions" : [], "centers" : [] }
		imageCrops = []

		for j in range(len(label)):
			thisLabel = label[j]
			# Don't train on objects without a class label
			if thisLabel["class"] == "DontCare":
				continue

			# Don't train/test on very occluded objects or truncated objects.
			if thisLabel["object_occluded"] >= 2:
				if thisLabel["object_truncated"] >= 0.75:
					continue
			elif thisLabel["object_occluded"] == 0:
				if thisLabel["object_truncated"] >= 0.90:
					continue

			instanceCrop, instanceLabel, rot_y = self.process_one_instance(image, thisLabel, binsStarts, binsOverlap)
			assert len(instanceLabel.keys()) == len(imageLabels.keys())
			for key in instanceLabel:
				imageLabels[key].append(instanceLabel[key])
			imageCrops.append(instanceCrop)

			# Data augmentation only in training mode
			if testingMode == False:
				# This may be VERY wrong. Alpha angle may not be correctly computed and need further attention. TODO.
				if "mirror" in self.dataAugmentation:
					reversedCrop = instanceCrop[:, ::-1, :]
					newLabel = deepcopy(instanceLabel)

					# Do same thing as before (-180 ; 180) to (0 ; 360)
					alpha_angle = thisLabel["alpha_angle"]
					if alpha_angle < 0:
						alpha_angle += 2 * np.pi
					# But also add 180 more degress due to rotation.
					alpha_angle += np.pi
					alpha_angle %= 2 * np.pi
					assert alpha_angle >= 0 and alpha_angle <= 2 * np.pi

					binsDeplacements, binsConfidence = computeBinDeplacements(alpha_angle, binsStarts, binsOverlap)
					sinDisplacements = np.sin(binsDeplacements)
					cosDisplacements = np.cos(binsDeplacements)
					# The first array has shape 2 x numBins, but we need numBins x 2 for loss computatiion.
					displacements = np.swapaxes(np.array([sinDisplacements, cosDisplacements]), 0, 1)
					newLabel["alphaAngle"] = alpha_angle
					newLabel["orientations"] = displacements

					for key in newLabel:
						imageLabels[key].append(newLabel[key])
						imageCrops.append(reversedCrop)
					# plot_image(instanceCrop)
					# plot_image(reversedCrop)
					# plot_image(image)
					# print("Instance alpha angle:", radToDeg(instanceLabel["alphaAngle"]), "reversed alpha angle:", radToDeg(newLabel["alphaAngle"]))
					# print(radToDeg(rot_y))
					# show_plots()

		return imageCrops, imageLabels

	def iterate(self, type, miniBatchSize, numBins, binsOverlap, testingMode=False, permutated=False):
		assert type in ("train", "validate")
		binsStarts = computeBinsStarts(numBins, binsOverlap)

		keys = ["size", "orientations", "confidence", "classIndex", "alphaAngle", "bbox_positions", "centers"]
		while True:
			for items in super().iterate(type, miniBatchSize, permutated=permutated):
				itemsLabels = {"size" : [], "orientations" : [], "confidence" : [], "classIndex" : [], \
					"alphaAngle" : [], "bbox_positions" : [], "centers" : [] }
				crops = []

				images, labels, calibration = items
				for i in range(len(images)):
					cropsImage, labelsImage = self.process_one_image(images[i], labels[i], binsStarts, binsOverlap, \
						testingMode)
					assert len(labelsImage.keys()) == len(itemsLabels.keys())
					for key in labelsImage:
						itemsLabels[key].extend(labelsImage[key])
					crops.extend(cropsImage)

				# Transform them all into numpy arrays
				for key in itemsLabels:
					itemsLabels[key] = np.array(itemsLabels[key])

				if testingMode == False:
					boxes_labels = [itemsLabels["size"], itemsLabels["orientations"], itemsLabels["confidence"]]
				else:
					# Ordering of keys matters here!!!
					boxes_labels = []
					for key in keys:
						boxes_labels.append(itemsLabels[key])
					boxes_labels.append(images)
					boxes_labels.append(calibration)
				yield np.array(crops), boxes_labels

# Given an angle (from camera to object) and a list of bin starts + overlapping between them, compute the
#  angle from the bin starts to the object and the confidence (1 if the bin reaches the angle, 0 otherwsie).
def computeBinDeplacements(angle, binsStarts, binsOverlap):
	degAngle = radToDeg(angle)
	numBins = len(binsStarts)
	binsConfidence = np.zeros((numBins, ), dtype=int)

	binsDisplacements = degToRad((binsStarts - degAngle) % 360)
	debug = False

	if debug:
		print("Bins displacements:", binsDisplacements)
		print("Computing for angle (deg):", degAngle, "(rad): ", angle)
		print("Bins starts (deg):", degToRad(binsStarts), "(rad):", binsStarts)
		print("bins Overlap:", binsOverlap)

	if numBins == 1:
		binsConfidence[0] = 1
		return binsDisplacements, binsConfidence

	# for an overlap of 20 degrees, at 2 bins, the value is (360 / 2) + (20 / 2) = 190 degrees step. The first 10
	#  degrees are an overlap of bin i with bin i + 1. The other 10 degrees are at the start, when bin i - 1 overlaps
	#  with this bin for 10 degrees.
	stepAngle = (360 // numBins) + (binsOverlap // 2)
	for i in range(numBins):
		# First part is the normal (90 < 130 < 145). Second is for when it goes over 360: (340 < 5 < 30)
		binEnd = (binsStarts[i] + stepAngle) % 360
		# In both cases binEnd >= degAngle (145 > 130 and 30 > 5).
		# In first case 90 < 130. In second case (340 > 5 and 340 > 30).
		# 340 -> 30
		if binsStarts[i] > binEnd:
			# 340 -> 355 -> 30 OR 340 -> 5 -> 30
			if (binsStarts[i] <= degAngle and binEnd <= degAngle) or \
				(binsStarts[i] >= degAngle and binEnd >= degAngle):
				binsConfidence[i] = 1
		# 90 -> 145
		else:
			# 90 -> 130 -> 145
			if binsStarts[i] <= degAngle and binEnd >= degAngle:
				binsConfidence[i] = 1

	if debug:
		print("Bins confidence:", binsConfidence)

	# If more than 1 bin is included, the confidence is split between them (all are supposed to be regressed)
	# [1, 1, 0, 0, 1] => [0.33, 0.33, 0, 0, 0.33]
	binsConfidence = binsConfidence / np.sum(binsConfidence)
	return binsDisplacements, binsConfidence

# Computes the static beginning angle of each bin
# i.e for 2 bins and 10 overlap we get [-355, 175], meaning that first bin start at angle -355 and goes until
#  185 (360/2 + 10 => 10 overlap). Same for second, it starts from 175 and goes to 5.
def computeBinsStarts(numBins, binsOverlap):
	if numBins == 1:
		return np.array([0])

	starts = np.zeros((numBins, ))
	for i in range(numBins):
		start = ((360 // numBins) * i - (binsOverlap // 4)) % 360
		starts[i] = start
	return starts

# Taken from internets, to rewrite.
def get_3d_points(dims, rot_y, center, calibration, imageShape):
	# TODO
	# points = np.ones((2, 2, 2, 3)) # 3d points
	# height, width, length = sizes
	# points[:, :, 0, Z] = center[Z] - (height / 2)
	# points[:, :, 1, Z] = center[Z] + (height / 2)
	# points[:, 0, :, Y] = center[Y] - (length / 2)
	# points[:, 1, :, Y] = center[Y] + (length / 2)
	# points[0, :, :, X] = center[X] - (width / 2)
	# points[1, :, :, X] = center[X] + (width / 2)
	# points = points.reshape((8, 3))
	# points = np.dot(toHomogeneousData(points), HRotationY(-alphaAngleResults[i]).matrix)

	# projected_points = np.dot(calibration, points.T).T
	# projected_points[:, 0 : 2] /= projected_points[:, 2].reshape((8, 1))
	# projected_points = projected_points[:, 0 : 2].astype(np.int16)
	# print(projected_points)
	# Bottom
	# points[:, :, 0, 0] = center[]

	box_points = []
	for i in [1, -1]:
		for j in [1, -1]:
			for k in [0, 1]:
				point = np.copy(center)
				point[0] = center[0] + i * dims[1]/2 * np.cos(-rot_y+np.pi/2) + (j*i) * dims[2]/2 * np.cos(-rot_y)
				point[2] = center[2] + i * dims[1]/2 * np.sin(-rot_y+np.pi/2) + (j*i) * dims[2]/2 * np.sin(-rot_y)
				point[1] = center[1] - k * dims[0]

				point = np.append(point, 1)
				point = np.dot(calibration, point)
				point = point[0 : 2] / point[2]

				box_points.append(point)
	box_points = np.round(np.array(box_points)).astype(np.int32)

	# Now, after we got the nodes, get the edges between them as tuple of points.
	drawPoints = []
	for i in range(4):
		drawPoints.append((box_points[2 * i], box_points[2 * i + 1]))
	for i in range(8):
		drawPoints.append((box_points[i], box_points[(i + 2) % 8]))

	return drawPoints

def test(model, datasetReader):
	binsStarts = degToRad(computeBinsStarts(model.numBins, model.binsOverlap))
	miniBatchSize = 5
	numSteps = datasetReader.getNumberOfSteps("validate", miniBatchSize)

	# Store a dictionary of final results for all classes
	# [0]: orientation; [1 : 3]: sizes, [4]: counts
	finalSizes = np.zeros((datasetReader.getNumberOfClasses(), 3), dtype=np.float32)
	finalOrientations = np.zeros((datasetReader.getNumberOfClasses(), 1), dtype=np.float32)
	finalCounts = np.zeros((datasetReader.getNumberOfClasses(), 1), dtype=np.float32)

	print("\nTesting for", numSteps, "steps with a mini batch of:", miniBatchSize)
	for step, items in enumerate(datasetReader.iterate("validate", miniBatchSize=miniBatchSize, numBins=model.numBins,\
		binsOverlap=model.binsOverlap, testingMode=True)):

		images, labels = items
		results = model.predict(images)
		numBoxes = len(labels[0])
		assert numBoxes == len(labels[1]) and numBoxes == len(labels[2]) and numBoxes == len(labels[3]) \
			and len(labels[7]) == miniBatchSize

		sizesLabels, orientationsLabels, confidenceLabels, classIndexLabels, alphaAngleLabels, _, _, _, _ = labels
		sizesResults, orientationsResults, confidenceResults = results

		for i in range(numBoxes):
			# Get the orientation w.r.t the biggest confidence
			confidenceArgmax = np.argmax(confidenceResults[i])
			orientationArgmax = orientationsResults[i][confidenceArgmax]
			assert np.abs(orientationArgmax[0]**2 + orientationArgmax[1]**2 - 1) < 0.001
			orientationArgmax = np.clip(orientationArgmax, -1, 1)
			alphaAngleResult = np.arccos(orientationArgmax[1]) * np.sign(orientationArgmax[0])
			alphaAngleResult = (binsStarts[confidenceArgmax] - alphaAngleResult) % (2 * np.pi)
			objectClass = datasetReader.getObjectClassByIndex(classIndexLabels[i])

			sizesDifference = np.abs(sizesResults[i] - sizesLabels[i])
			orientationDifference = np.abs(orientationsResults[i] - orientationsLabels[i])
			confidenceDifference = np.abs(confidenceResults[i] - confidenceLabels[i])
			alphaAngleDifference = np.abs(alphaAngleResult - alphaAngleLabels[i]) % 2 * np.pi
			if alphaAngleDifference > np.pi:
				alphaAngleDifference = 2 * np.pi - alphaAngleDifference

			# Train is done by minimizing the average sizes, so add it back.
			sizesLabels[i] += datasetReader.getObjectSizeMeans(objectClass)
			sizesResults[i] += datasetReader.getObjectSizeMeans(objectClass)

			finalSizes[classIndexLabels[i]] += sizesDifference
			finalOrientations[classIndexLabels[i]] += radToDeg(alphaAngleDifference)
			finalCounts[classIndexLabels[i]] += 1
		step += 1
		if step == numSteps:
			break

	print("Final results:")
	for i in range(datasetReader.getNumberOfClasses()):
		if finalCounts[i] > 0:
			finalSizes[i] /= finalCounts[i]
			finalOrientations[i] /= finalCounts[i]
			objectClass = datasetReader.getObjectClassByIndex(i)
			print(objectClass, "=> Sizes: ", finalSizes[i], "=> Orientation: ", finalOrientations[i])

def train(model, datasetReader, miniBatchSize, numEpochs):
	train_generator = datasetReader.iterate("train", miniBatchSize, model.numBins, model.binsOverlap)
	validation_generator = datasetReader.iterate("validate", miniBatchSize, model.numBins, model.binsOverlap)

	steps = datasetReader.getNumberOfSteps("train", miniBatchSize)
	# validation_steps = datasetReader.getNumberOfSteps("validate", miniBatchSize)
	validation_steps = 10
	
	testCallback = LambdaCallback(
		on_epoch_end=lambda epoch, logs : test(model, datasetReader)
	)

	callbacks = [ModelCheckpoint("weights.{epoch:02d}-{val_loss:.2f}.hdf5"), testCallback]
	print("Training. Epochs:", numEpochs ,"MiniBatch size:", miniBatchSize, "Training steps:", steps, \
		"Validation steps:", validation_steps)
	model.fit_generator(generator=train_generator, steps_per_epoch=steps, validation_data=validation_generator, \
		validation_steps=validation_steps, epochs=numEpochs, callbacks=callbacks)

def test_one(model, datasetReader):
	binsStarts = degToRad(computeBinsStarts(model.numBins, model.binsOverlap))
	miniBatchSize = 1
	numSteps = datasetReader.getNumberOfSteps("validate", 1)
	import matplotlib.pyplot as plt

	changeDirectory("results", force=True)
	for ind, items in enumerate(datasetReader.iterate("validate", miniBatchSize=miniBatchSize, numBins=model.numBins,\
		binsOverlap=model.binsOverlap, testingMode=True, permutated=True)):
		images, labels = items

		results = model.predict(images)
		numBoxes = len(labels[0])

		sizesLabels, orientationsLabels, confidenceLabels, classIndexLabels, alphaAngleResults, bboxLabels2D, \
			centerLabels, originalImage, calibration = labels
		originalImage = originalImage[0]
		calibration = calibration[0]
		assert calibration.shape == (3, 4)
		sizesResults, orientationsResults, confidenceResults = results

		assert numBoxes == len(sizesLabels) and numBoxes == len(orientationsLabels) and \
			numBoxes == len(confidenceLabels) and numBoxes == len(bboxLabels2D)
		# plot_image(originalImage, title="Original")

		assert len(bboxLabels2D[0]) == 2 and len(bboxLabels2D[0][0]) == 2
		newImage = addSquares(originalImage, bboxLabels2D, copy=True)
		# plot_image(newImage, title="2D boxed")

		newImage_3D = np.copy(originalImage)
		realResults = True

		for i in range(numBoxes):
			# Get the orientation w.r.t the biggest confidence
			confidenceArgmax = np.argmax(confidenceResults[i])
			orientationArgmax = orientationsResults[i][confidenceArgmax]
			assert np.abs(orientationArgmax[0]**2 + orientationArgmax[1]**2 - 1) < 0.001

			alphaAngleResult = np.arccos(orientationArgmax[1]) * np.sign(orientationArgmax[0])
			alphaAngleResult = (binsStarts[confidenceArgmax] - alphaAngleResult) % (2 * np.pi)
			alphaAngleDifference = np.abs(alphaAngleResult - alphaAngleResults[i]) % 2 * np.pi
			if alphaAngleDifference > np.pi:
				alphaAngleDifference = 2 * np.pi - alphaAngleDifference

			# print(confidenceArgmax, "vs", np.argmax(confidenceLabels[i]), "with value:", np.max(confidenceResults[i]))
			print("Alpha angle difference from label:", radToDeg(alphaAngleDifference))
			print("Sizes difference from label:", np.abs(sizesResults[i] - sizesLabels[i]))

			# sizes: H, W, L
			sizesResults[i] += datasetReader.getObjectSizeMeans(datasetReader.getObjectClassByIndex(classIndexLabels[i]))
			sizesLabels[i] += datasetReader.getObjectSizeMeans(datasetReader.getObjectClassByIndex(classIndexLabels[i]))

			# Still use labeled center.
			center = centerLabels[i] # x, y, z
			box_3d_result = get_3d_points(sizesResults[i], alphaAngleResult, center, calibration, originalImage.shape)
			# box_3d_label = get_3d_points(sizesLabels[i], alphaAngleResults[i], center, calibration, originalImage.shape)
			if radToDeg(alphaAngleDifference) < 30 or radToDeg(alphaAngleDifference) > 330:
				addLines(newImage_3D, box_3d_result, color=(0, 255, 0), copy=False, method="remove_outliers")
			else:
				addLines(newImage_3D, box_3d_result, color=(255, 0, 0), copy=False, method="remove_outliers")

		print("---------")
		fig = plt.figure(figsize=(newImage_3D.shape[1] / 80, newImage_3D.shape[0] / 80))
		ax = fig.add_axes([0, 0, 1, 1])
		ax.axis("off")
		plt.imshow(misc.toimage(newImage_3D))
		show_plots(block=True)
		# plt.savefig("%i.png" % ind, dpi=80)
		# if ind == 150:
			# break

def main():
	np.seterr("raise")
	args = parseArguments()
	datasetReader = CustomKITTIReader(args.dataset_path, splitTrainValidate=args.split_train_validate,\
		splitPercent=args.split_train_validate_percent, dataAugmentation=[])

	K.set_learning_phase(1 if args.type in ("train", "retrain") else 0)
	model = Model3D("vgg16", args.num_neurons, args.num_neurons, args.num_neurons, args.orientation_num_bins,\
		args.orientation_bins_overlap, args.dropout, args.batch_normalization)

	if args.type == "test":
		model.load_state(args.state_file)
		model.load_weights(args.weights_file)
		test(model, datasetReader)
	elif args.type == "train":
		changeDirectory(args.dir)
		model.save_state(args.state_file)
		model.setOptimizer(SGD(lr=0.001))
		model.summary()
		model.compile()
		train(model, datasetReader, args.mini_batch_size, args.num_epochs)
	elif args.type == "retrain":
		changeDirectory(args.dir)
		model.load_state(args.state_file)
		model.load_weights(args.weights_file)
		model.setOptimizer(SGD(lr=0.001))
		model.summary()
		model.compile()
		train(model, datasetReader, args.mini_batch_size, args.num_epochs)
	elif args.type == "test_one":
		model.load_state(args.state_file)
		model.load_weights(args.weights_file)
		test_one(model, datasetReader)

if __name__ == "__main__":
	main()
