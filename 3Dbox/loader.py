# loader.py Defines public functions to use a model with one labeled image.
from .models import getModel
import os

weights_path = os.path.dirname(os.path.realpath(__file__)) + os.sep +  "train_1_full_mobilenet/weights.26--0.06.hdf5"
# By default this is None, after getModel is called once it is initialized (tf.session), so we don't load it every time
model = None

def loadModel():
	global model
	numOrientations = 2
	numUnits = (100, 100, 100)
	model = getModel(numOrientations, *numUnits)
	model.load_weights(weights_path)
	print("3d model loaded.")

def updateImage(image):
	global model


def get2DBox(image):
	global model
	if model == None:
		loadModel()