import importlib
from argparse import ArgumentParser
from Mihlib import *
from datetime import datetime
from random import randint

_2DModule = importlib.import_module("2Dbox.picker")
_2DModule.loadModel()
_3DModule = importlib.import_module("3Dbox.loader")
_3DModule.loadModel()

def parseArguments():
	parser = ArgumentParser()
	parser.add_argument("type", default="dataset", type=str)
	parser.add_argument("path", type=str)
	parser.add_argument("--split_train_validate", default=0, type=int)
	parser.add_argument("--split_train_validate_percent", default=5, type=float)

	args = parser.parse_args()
	assert args.type in ("dataset", "image")
	assert args.split_train_validate in (0, 1)

	args.split_train_validate = True if args.split_train_validate == 1 else False
	if args.split_train_validate:
		assert args.split_train_validate_percent > 0 and args.split_train_validate_percent < 100

	return args

def box2DImage(image):
	now = datetime.now()
	res = _2DModule.get2DBox(_2DModule.updateImage(image))
	print("2d box computation took {}".format(datetime.now() - now))

	boxes, probabilities, classes = res
	newImage = np.copy(image)
	print(boxes)
	addSquares(newImage, boxes, copy=False)
	plot_image(newImage)
	return boxes, probabilities, classes

def main():
	args = parseArguments()

	if args.type == "dataset":
		datasetReader = KITTIReader(args.path, splitTrainValidate=args.split_train_validate)
		mbSize = 100
		for items in datasetReader.iterate("train", miniBatchSize=mbSize):
			for item in range(mbSize):
				images, labels = items
				image = images[item]
				box2DImage(image)
	else:
		image = readImage(args.path)
		boxes, probabilities, classes = box2DImage(image)
		print(boxes)
		print(probabilities)
		print(classes)

		# TODO 3D, because i don't have access to alpha angle.


	show_plots()

if __name__ == "__main__":
	main()