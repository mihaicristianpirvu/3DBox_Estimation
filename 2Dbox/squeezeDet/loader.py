import os
import cv2
import numpy as np
import tensorflow as tf

from .src.config import *
from .src.nets import *

model_path = os.path.dirname(os.path.realpath(__file__)) + "/data/model_checkpoints/squeezeDet/model.ckpt-87000"
# By default this is None, after getModel is called once it is initialized (tf.session), so we don't load it every time
model = None
sess = None
mc = None

def loadModel():
	global model
	global sess
	global mc
	if model == None:
		sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
		# Load model
		mc = kitti_squeezeDet_config()
		mc.BATCH_SIZE = 1
		mc.LOAD_PRETRAINED_MODEL = False
		model = SqueezeDet(mc)
		saver = tf.train.Saver(model.model_params)
		saver.restore(sess, model_path)

def updateImage(image):
	global model
	global mc
	if model == None:
		loadModel()
	im = image.astype(np.float32, copy=False)
	im = cv2.resize(im, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT))
	input_image = im - mc.BGR_MEANS
	return input_image

def get2DBox(image):
	global model
	global sess
	global mc
	if model == None:
		loadModel()

	# Detect
	det_boxes, det_probs, det_class = sess.run(
		[model.det_boxes, model.det_probs, model.det_class],
		feed_dict={model.image_input:[image]})

	# Filter
	final_boxes, final_probs, final_class = model.filter_prediction(
		det_boxes[0], det_probs[0], det_class[0])

	keep_idx    = [idx for idx in range(len(final_probs)) \
					  if final_probs[idx] > mc.PLOT_PROB_THRESH]
	final_boxes = [final_boxes[idx] for idx in keep_idx]
	final_probs = [final_probs[idx] for idx in keep_idx]
	final_class = [final_class[idx] for idx in keep_idx]

	# Fix so they return (top_left, bottom_right) instead of (center, half(???) distance to corner)
	fixed_boxes = np.zeros((len(final_boxes), 2, 2), dtype=int)
	for i in range(len(final_boxes)):
		top_left = (final_boxes[i][1] - (final_boxes[i][3] / 2), final_boxes[i][0] - (final_boxes[i][2] / 2))
		bottom_right = (final_boxes[i][1] + (final_boxes[i][3] / 2), final_boxes[i][0] + (final_boxes[i][2] / 2))
		fixed_boxes[i] = top_left, bottom_right
	return fixed_boxes, final_probs, final_class