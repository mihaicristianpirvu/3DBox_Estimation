from Mihlib import NeuralNetworkKeras
from keras.applications.vgg16 import VGG16
from keras.applications.mobilenet import MobileNet
from keras.models import Model
from keras.layers import Flatten, Dense, Input, Reshape, Lambda, BatchNormalization, Dropout
from keras.optimizers import SGD
from keras import backend as K

class Model3D(NeuralNetworkKeras):
	def __init__(self, baseType, unitsDimensions=0, unitsOrientation=0, unitsConfidence=0, numBins=0, binsOverlap=0, \
		dropout=1, batchNormalization=False):
		assert baseType in ("dummy", "mobilenet", "vgg16")
		assert dropout > 0 and dropout <= 1
		self.baseType = baseType
		self.unitsDimensions = unitsDimensions
		self.unitsOrientation = unitsOrientation
		self.unitsConfidence = unitsConfidence
		self.numBins = numBins
		self.binsOverlap = binsOverlap
		self.dropout = dropout
		self.batchNormalization = batchNormalization
		super().__init__()

	@staticmethod
	def dimensionLoss(y_true, y_predict):
		# L2 loss for all the 3 sizes
		return K.mean(K.square(y_predict - y_true), axis=-1)

	@staticmethod
	def confidenceLoss(y_true, y_predict):
		# L2 loss for all the N bins confidence
		return K.mean(K.square(y_predict - y_true), axis=-1)

	@staticmethod
	def orientationLoss(y_true, y_predict):
		anchors = K.sum(K.square(y_true), axis=2)
		anchors = K.greater(anchors, K.constant(0.5))
		anchors = K.sum(K.cast(anchors, "float32"), axis=1)

		# Define the loss
		loss = -(y_true[:, :, 0] * y_predict[:, :, 0] + y_true[:, :, 1] * y_predict[:, :, 1])
		loss = K.sum(loss, axis=1)
		loss = loss / anchors

		return K.mean(loss)

	def setup(self):
		if self.baseType == "vgg16":
			baseModel = VGG16(weights="imagenet", include_top=False, input_shape=(224, 224, 3))
		elif self.baseType == "mobilenet":
			baseModel = MobileNet(weights="imagenet", include_top=False, input_shape=(224, 224, 3))
		else:
			inputs = Input(shape=(224, 224, 3))
			baseModel = Model(inputs=inputs, outputs=inputs)
			baseModel.layers = []

		for layer in baseModel.layers:
			layer.trainable = False

		x = Flatten()(baseModel.output)
		out_dimensions = Dense(self.unitsDimensions, activation="relu", name="fc_1_dimensions")(x)
		if self.batchNormalization:
			out_dimensions = BatchNormalization()(out_dimensions)
		if self.dropout != 1:
			out_dimensions = Dropout(self.dropout)(out_dimensions)
		out_dimensions = Dense(3, name="dimensions")(out_dimensions)

		out_orientation = Dense(self.unitsOrientation, activation="relu", name="fc_1_orientation")(x)
		if self.batchNormalization:
			out_orientation = BatchNormalization()(out_orientation)
		if self.dropout != 1:
			out_orientation = Dropout(self.dropout)(out_orientation)
		out_orientation = Dense(2 * self.numBins)(out_orientation)
		out_orientation = Reshape((self.numBins, 2))(out_orientation)
		out_orientation = Lambda(lambda x : K.l2_normalize(x, axis=2), name="orientations")(out_orientation)

		out_confidence = Dense(self.unitsConfidence, activation="relu", name="fc_1_confidence")(x)
		if self.batchNormalization:
			out_confidence = BatchNormalization()(out_confidence)
		if self.dropout != 1:
			out_confidence = Dropout(self.dropout)(out_confidence)
		out_confidence = Dense(self.numBins, name="confidence")(out_confidence)

		self.setCriterion({"dimensions" : Model3D.dimensionLoss, "orientations" : Model3D.orientationLoss, \
			"confidence" : Model3D.confidenceLoss})
		self.setIO(inputs=baseModel.inputs, outputs=[out_dimensions, out_orientation, out_confidence])
